from django.contrib import admin
from models import *


class PlayerAdmin(admin.ModelAdmin):
    model = Player
    list_display = ('lineupChallenge', 'name', 'salary', 'position')
    fields = ('lineupChallenge', 'firstName', 'lastName', 'salary', 'position')
    search_fields = ['salary', 'position']
    list_filter = ['lineupChallenge', 'position',]
admin.site.register(Player, PlayerAdmin)


class PlayerTabularInline(admin.TabularInline):
    model = Player
    extra = 1
    fk_name = "lineupChallenge"


class FantasySportPlayerLineupChallengeAdmin(admin.ModelAdmin):
    model = FantasySportPlayerLineupChallenge
    list_display = ('id', 'salaryCap', 'salaryCapRemaining',)
    readonly_fields = ('id', 'salaryCapRemaining')
    fields = ('id', 'salaryCap', 'salaryCapRemaining',)
    search_fields = ['salaryCapRemaining']
    inlines = [PlayerTabularInline]

admin.site.register(FantasySportPlayerLineupChallenge, FantasySportPlayerLineupChallengeAdmin)

