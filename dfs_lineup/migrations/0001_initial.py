# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import model_utils.fields
import django.utils.timezone
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FantasySportPlayerLineupChallenge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('salaryCap', models.DecimalField(verbose_name=b'The total salary cap, in dollars', max_digits=22, decimal_places=2, validators=[django.core.validators.MinValueValidator(0.0)])),
                ('salaryCapRemaining', models.DecimalField(decimal_places=2, editable=False, max_digits=22, validators=[django.core.validators.MinValueValidator(0.0)], null=True, verbose_name=b'The remaining salary cap, in dollars')),
            ],
            options={
                'ordering': ['-id'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('firstName', models.CharField(max_length=50, verbose_name=b"The player's first name")),
                ('lastName', models.CharField(max_length=50, verbose_name=b"The player's last name")),
                ('salary', models.DecimalField(verbose_name=b"The player's salary in dollars", max_digits=20, decimal_places=2, validators=[django.core.validators.MinValueValidator(0.0)])),
                ('position', models.CharField(max_length=20, verbose_name=b'The players position', choices=[(b'QB', b'Quarterback'), (b'RB', b'Runningback'), (b'WR', b'Wide Receiver'), (b'TE', b'Tightend')])),
                ('lineup', models.ForeignKey(related_name='lineup', editable=False, to='dfs_lineup.FantasySportPlayerLineupChallenge', null=True)),
                ('lineupChallenge', models.ForeignKey(related_name='players', to='dfs_lineup.FantasySportPlayerLineupChallenge')),
            ],
            options={
                'ordering': ['lineupChallenge', 'position', '-salary'],
            },
            bases=(models.Model,),
        ),
    ]
