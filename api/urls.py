from django.conf.urls import url, include, patterns
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'lineup_challenges', views.FantasySportPlayerLineupChallengeViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = patterns('api',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
