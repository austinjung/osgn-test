import random
from rest_framework.exceptions import ValidationError, ParseError, APIException
from itertools import izip

class Player(object):
    """
    Fantasy Sports players and their salaries
    """
    POSITIONS = ["QB", "RB", "WR", "TE"]
    def __init__(self, id=0, salary=0, position='TE'):
        self.name = 'Player%d' % id
        self.salary = salary
        self.position = position

    def __str__(self):
        return '%s[%s] $%d' % (self.name, self.position, self.salary)

    @classmethod
    def generate_players_equally(cls):
        players = []
        for id in range(1, 51, 1):
            players.append(cls(id=id, salary=random.randint(10, 100), position=cls.POSITIONS[random.randint(0, 3)]))
        return players

    @classmethod
    def generate_players_not_equally(cls):
        players = []
        for id in range(1, 11, 1):
            players.append(cls(id=id, salary=random.randint(100, 200), position='QB'))
        for id in range(1, 16, 1):
            players.append(cls(id=id, salary=random.randint(75, 150), position='RB'))
        for id in range(1, 16, 1):
            players.append(cls(id=id, salary=random.randint(75, 150), position='WR'))
        for id in range(1, 21, 1):
            players.append(cls(id=id, salary=random.randint(50, 100), position='TE'))
        return players

    @classmethod
    def generate_players_without_TE(cls):
        players = []
        for id in range(1, 51, 1):
            players.append(cls(id=id, salary=random.randint(10, 100), position=cls.POSITIONS[random.randint(0, 2)]))
        return players


class FantasySportPlayerLineupChallenge(object):
    """
    Inputs: A list of Fantasy Sports players and their salaries plus a salary cap.
    """
    LINEUP_POSITIONS = ["QB", "RB1", "RB2", "WR1", "WR2", "TE", "FLEX"]
    def __init__(self, salaryCap=0, generate_player_method='generate_players_equally'):
        self.salaryCap = salaryCap
        self.players = getattr(Player, generate_player_method)()

    def __str__(self):
        return 'Cap = $%d\nQB Players :\n\t%s\nRB Players\n\t%s\nWR Players\n\t%s\nTE Players\n\t%s\nFLEX pools\n\t%s' % (self.salaryCap,
                                             reduce(lambda x, y: '%s\n\t%s' % (str(x), str(y)), self.QB),
                                             reduce(lambda x, y: '%s\n\t%s' % (str(x), str(y)), self.RB1),
                                             reduce(lambda x, y: '%s\n\t%s' % (str(x), str(y)), self.WR1),
                                             reduce(lambda x, y: '%s\n\t%s' % (str(x), str(y)), self.TE),
                                             reduce(lambda x, y: '%s\n\t%s' % (str(x), str(y)), self.FLEX))

    def categorizePlayerPoolAndSortBySalaryDec(self):
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            setattr(self, position, [])
        for player in self.players:
            if player.position == 'QB':
                getattr(self, player.position, []).append(player)
            elif player.position == 'TE':
                getattr(self, player.position, []).append(player)
                getattr(self, "FLEX", []).append(player)
            else:
                getattr(self, "%s1" % player.position, []).append(player)
                getattr(self, "%s2" % player.position, []).append(player)
                getattr(self, "FLEX", []).append(player)
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            setattr(self, position, sorted(getattr(self, position, []), key=lambda x: x.salary, reverse=True))
        self.number_of_players_in_positions = {'max': 0}
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            number_of_players_in_position = len(getattr(self, position, []))
            self.number_of_players_in_positions[position] = number_of_players_in_position
            if number_of_players_in_position == 0:
                raise ValidationError("There in no %s in players pool." % position)
            elif number_of_players_in_position > self.number_of_players_in_positions['max']:
                self.number_of_players_in_positions['max'] = number_of_players_in_position

    def roughlyCutPlayersByCap(self):
        for index in range(0, self.number_of_players_in_positions['max'], 1):
            self.salaryCapRemaining = self.salaryCap
            self.lineup_pool = []
            self.roughCutIndexes = {}
            for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
                for sub_index in range(index, self.number_of_players_in_positions[position], 1):
                    player = getattr(self, position, [])[sub_index]
                    if player not in self.lineup_pool:
                        self.lineup_pool.append(player)
                        self.roughCutIndexes[position] = sub_index
                        self.salaryCapRemaining -= getattr(self, position, [])[sub_index].salary
                        break
            if self.salaryCapRemaining >= 0 and len(self.lineup_pool) == 7:
                self.salaryCapRemaining = self.salaryCapRemaining
                print "Rough Cut with $%d remaining" % self.salaryCapRemaining
                for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
                    print getattr(self, position, [])[self.roughCutIndexes[position]]
                break
        if len(self.lineup_pool) < 7 or self.salaryCapRemaining < 0:
            raise APIException("Cannot make a lineup with salary cap.")

    def fineCutAfterRoughCut(self):
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            if self.roughCutIndexes[position] > 0:
                diff = getattr(self, position, [])[self.roughCutIndexes[position] - 1].salary \
                                           - getattr(self, position, [])[self.roughCutIndexes[position]].salary
                if self.salaryCapRemaining >= diff and \
                        getattr(self, position, [])[self.roughCutIndexes[position] - 1] not in self.lineup_pool:
                    self.salaryCapRemaining -= diff
                    self.lineup_pool.remove(getattr(self, position, [])[self.roughCutIndexes[position]])
                    self.lineup_pool.append(getattr(self, position, [])[self.roughCutIndexes[position] - 1])
                else:
                    return
            else:
                continue
        self.fineCutAfterRoughCut()


if __name__ == "__main__":
    # There in no TE in players pool.
    challenge = FantasySportPlayerLineupChallenge(salaryCap=700, generate_player_method='generate_players_without_TE')
    try:
        challenge.categorizePlayerPoolAndSortBySalaryDec()
        print challenge
        challenge.roughlyCutPlayersByCap()
        challenge.fineCutAfterRoughCut()
        print "Fine Cut with $%d remaining" % challenge.salaryCapRemaining
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            print getattr(challenge, position, [])[challenge.roughCutIndexes[position]]
    except (ValidationError, APIException) as e:
        print e.detail

    # Cannot make a lineup with salary cap.
    challenge = FantasySportPlayerLineupChallenge(salaryCap=300, generate_player_method='generate_players_not_equally')
    try:
        challenge.categorizePlayerPoolAndSortBySalaryDec()
        print challenge
        challenge.roughlyCutPlayersByCap()
        challenge.fineCutAfterRoughCut()
        print "Fine Cut with $%d remaining" % challenge.salaryCapRemaining
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            print getattr(challenge, position, [])[challenge.roughCutIndexes[position]]
    except (ValidationError, APIException) as e:
        print e.detail

    challenge = FantasySportPlayerLineupChallenge(salaryCap=500, generate_player_method='generate_players_equally')
    try:
        challenge.categorizePlayerPoolAndSortBySalaryDec()
        print challenge
        challenge.roughlyCutPlayersByCap()
        challenge.fineCutAfterRoughCut()
        print "Fine Cut with $%d remaining" % challenge.salaryCapRemaining
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            print getattr(challenge, position, [])[challenge.roughCutIndexes[position]]
    except (ValidationError, APIException) as e:
        print e.detail

    challenge = FantasySportPlayerLineupChallenge(salaryCap=900, generate_player_method='generate_players_not_equally')
    try:
        challenge.categorizePlayerPoolAndSortBySalaryDec()
        print challenge
        challenge.roughlyCutPlayersByCap()
        challenge.fineCutAfterRoughCut()
        print "Fine Cut with $%d remaining" % challenge.salaryCapRemaining
        for position in FantasySportPlayerLineupChallenge.LINEUP_POSITIONS:
            print getattr(challenge, position, [])[challenge.roughCutIndexes[position]]
    except (ValidationError, APIException) as e:
        print e.detail


